[Raspberry Pi camera module]: ./parts/raspberry_pi_camera_module.md
[M2x6mm screws]: ./parts/M2x6mm_screws.md
[1.5mm hex key]: ./parts/1_5mm_hex_key.md "{Cat: Tool}"
[camera mount]: ./parts/camera_mount.md "{Cat: PrintedPart}"
[M3x8mm screws]: ./parts/M3x8mm_screws.md
[2.5mm hex key]: ./parts/2_5mm_hex_key.md "{Cat: Tool}"
[long tube]: ./parts/long_tube.md "{Cat: PrintedPart}"
[30mm O-rings]: ./parts/30mm_o-rings.md
[white paper]: ./parts/white_paper.md
[LED mount]: ./parts/led_mount.md "{Cat: PrintedPart}"
[NeoPixel]: ./parts/neopixel.md
[wires with female jumper connectors]: ./parts/wires.md
[LED clamp]: ./parts/led_clamp.md "{Cat: PrintedPart}"
[soldering iron]: ./parts/soldering_iron.md "{Cat: Tool}"
[solder]: ./parts/solder.md
[wire strippers]: ./parts/wire_strippers.md "{Cat: Tool}"
[Arduino Mega]: ./parts/arduino_mega.md
[Raspberry Pi]: ./parts/raspberry_pi.md
[5V micro-USB power supply]: ./parts/pi_power_supply.md
[SD card]: ./parts/sd_card.md
[keyboard, monitor, and mouse]: ./parts/keyboard_monitor_mouse.md

# Raspberry Pi camera calibration jig
This document describes how to build a calibration jig that illuminates a Raspberry Pi camera module with uniform-intensity, approximately-collimated light.  Used in conjunction with a suitable RGB LED, it enables the sensor to be calibrated for colour response, allowing the vignetting and loss of saturation at the edge of the sensor to be compensated for in post-processing.  It can also be used to calculate a suitable Lens Shading Table under some circumstances.

{{BOM}}

# Assembly instructions
## Step 1: Remove the lens from the camera module
First, remove the lens from the [Raspberry Pi camera module]{Qty:1}.  This is described in the [OpenFlexure Microscope] assembly instructions, but just consists of using the plastic lens tool (included with v2 of the camera module) to unscrew the lens.  Be gentle, and take care not to damage the small ribbon cable connecting the sensor to the PCB.




## Step 2: Fit the camera module into the mount
Place the [camera module][Raspberry Pi camera module] on the flat side of the [camera mount]{Qty:1} and fix it in place with two [M2x6mm screws]{Qty:2}. You will need a [1.5mm hex key]{Qty:1}.

![](./images/camera_mount_back.jpg)
![](./images/camera_mount_front.jpg)
![](./images/camera_mount_parts.jpg)


## Step 3: Add screws to the tube
Screw four [M3x8mm screws]{Qty:4} into the [long tube]{Qty:1} using a [2.5mm hex key]{Qty:1}.

![](./images/tube_screws_parts.jpg)
![](./images/tube_screws.jpg)


## Step 4: Fit the camera mount into the tube
Place the camera mount into the [long tube], and secure using two [30mm O-rings]{Qty:2}, one either side.  The [30mm O-rings] are wrapped around the [M3x8mm screws] and the protruding parts of the camera mount.  Each O-ring is wrapped around twice, to ensure the mount is held tightly.

![](./images/mount_to_tube_back.jpg)
![](./images/mount_to_tube_side.jpg)
![](./images/mount_to_tube_parts.jpg)


## Step 5: Add the paper diffuser
Place the piece of [white paper]{Qty:1} on the bottom of the [long tube], then clamp it in place with the [LED mount]{Qty:1} and four [M3x8mm screws]{Qty:4}, using a [2.5mm hex key]{Qty:1}.

![](./images/diffuser_parts.jpg)
![](./images/diffuser_paper.jpg)
![](./images/diffuser_assembled.jpg)


## Step 6: Add the LED
Solder [wires with female jumper connectors]{Qty:3} to the data and power pads on the [NeoPixel]{Qty:1}. Clamp the [NeoPixel] onto the [LED mount] using the [LED clamp]{Qty:1} and two [M3x8mm screws]{Qty:2}, using a [2.5mm hex key]{Qty:1}.  You will probably need a [soldering iron]{Qty:1}, [solder]{Qty: a little}, and [wire strippers]{Qty:1}

![](./images/led_parts.jpg)
![](./images/led_assembled.jpg)

## Step 7: Connect to electronics
Connect the [NeoPixel] to the [Arduino Mega]{Qty:1}, wiring the 5V and 0V lines to ground and power on the Arduino, and the data line to digital pin 6.  Connect the [Raspberry Pi camera module] to the [Raspberry Pi]{Qty:1}.  The [arduino][Arduino Mega] doesn't need a powers supply as it's powered via USB from the Raspberry Pi, and the [Raspberry Pi] will require an [SD card]{Qty:1}, a [5V micro-USB power supply]{Qty:1}, and a [keyboard, monitor, and mouse]{Qty:1} or some other way of controlling it (e.g. a network cable). 

![](./images/jig_assembled.jpg)




[OpenFlexure Microscope]: https://www.openflexure.org/projects/microscope/