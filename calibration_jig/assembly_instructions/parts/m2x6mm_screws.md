# M2x6mm screws

## Details
*   **Supplier:** Anglian Fasteners Limited
*   **Supplier's part number:** 
*   **URL:** http://www.anglianfasteners.co.uk/
*   **Material units:** NONE

These screws attach the Raspberry Pi camera board to the optics module.  The exact details of the screws are unimportant, but the holes in the PCB are only 2mm in diameter.


