# Solder

You will need solder to connect the [wires](wires.md) to the [NeoPixel](neopixel.md).  This can be any solder you have to hand - no special requirements.
