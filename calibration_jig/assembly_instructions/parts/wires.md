# Wires with female header connector
You will need three wires, which are bare wire on one end (for soldering to the NeoPixel pads) and finished with a female header connector.  The easiest way to make these is by cutting a female-female jumper lead in half, though you can also crimp your own if you have a suitable crimping tool.
