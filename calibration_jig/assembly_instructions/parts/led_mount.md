# LED mount
The LED mount leaves a little space between the LED and the diffuser, so that the light can spread out a little and the colours can mix together (the three LEDs in the [NeoPixel] are not in exactly the same place).

[NeoPixel]: ./neopixel.md

## Printing instructions
Print the LED mount with the large flat side on the print bed.  No support material should be required - a brim may or may not be used, and if you use one you don't need to be too careful about removing it.

## STL Files
[``neopixel_adapter.stl``](../../stl/neopixel_adapter.stl)

