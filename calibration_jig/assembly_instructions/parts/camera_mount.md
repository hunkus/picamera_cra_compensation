# Camera mount
The camera mount is a semi-cylindrical block that mounts the camera, and allows it to be tilted within the calibration jig.  

## Printing instructions
The camera mount should be printed with the flat side on the print bed.  This means the camera mounts onto the flat bottom of the part.  It does not require support material, and if you use a brim you should be careful to remove it so it doesn't snag the mount or the delicate ribbon cable on the [camera module](./raspberry_pi_camera_module.md).

## STL Files
[``camera_holder.stl``](../../stl/camera_holder.stl)

