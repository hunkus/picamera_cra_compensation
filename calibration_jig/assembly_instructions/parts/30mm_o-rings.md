# Viton O ring, 30mm inner diameter, 2mm cross section

## Details
*   **Material:** Viton or Nitrile
*   **Inner diameter:** 30mm
*   **Cross-section:** 2mm
*   **Outer diameter:** 34mm
*   **Supplier website:** [http://www.simplybearings.co.uk/]

The O-rings function as springs, to pull the camera mount against the long tube.  I use Viton O-rings, 30mm inner diameter, as they provide about the right force.  You can substitute for rubber bands, but it's difficult to specify the size directly, and they tend to perish and snap in a few weeks.  These are the same viton o rings used in the openflexure microscope, so we had plenty available.



