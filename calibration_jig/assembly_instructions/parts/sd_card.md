# SD Card

The Raspberry Pi will need an SD card for the operating system.  This is a very standard part, available from many places e.g. [RS Components](https://uk.rs-online.com/web/p/products/7582587).  8Gb is plenty for this experiment.
