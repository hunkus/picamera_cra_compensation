# LED mount
The LED clamp fits over the [NeoPixel] and fixes it in place.

[NeoPixel]: ./neopixel.md

## Printing instructions
Print the LED clamp flat on the print bed - no support should be needed, but a brim might help it stick (and you don't need to be too careful about removing it).  One side of the clamp is flatter than the other - this side should be on the bottom.

## STL Files
[``neopixel_clamp.stl``](../../stl/neopixel_clamp.stl)

