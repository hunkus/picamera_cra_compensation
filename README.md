# Colour calibration for a Raspberry Pi camera

This repository contains the hardware (OpenSCAD/STL files and [build instructions](https://bath_open_instrumentation_group.gitlab.io/picamera_cra_compensation), software (Python scripts and Arduino firmware), data analysis (iPython notebook), and manuscript describing how to calibrate the colour response of a Raspberry Pi camera module.

* ``analysis`` contains the data analysis code.
* ``data`` contains the images that we used for the graphs in the manuscript.
* ``neopixel_driver`` is the arduino firmware.
* ``image_acquisition`` includes the Python code that acquired the images and controlled the neopixel.
* ``calibration_jig`` contains the printable files, source OpenSCAD files, and assembly instructions for the calibration jig.
* ``colour_test_sheet`` contains source Inkscape SVG files and PDF renders of the test target used in the experiments.
* ``manuscript`` contains the source files for the manuscript.

This is an open project, run by the Bath Open Instrumentation Group, part of the University of Bath.  Unless otherwise specified, all code is licensed under the GPL v3 or later, hardware is under the CERN open hardware license, and documentation/manuscript is CC-BY 3.0 or later.  The repository will be archived along with a published paper once it has been peer reviewed.  If you are viewing a static archive of these files, you may want to consult the [working repository], which may receive updates in the future.

[working repository]: https://gitlab.com/bath_open_instrumentation_group/picamera_cra_compensation